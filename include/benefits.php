<div class="py-5 text-white ben gradient-overlay" id="benifits" class="parallax" class="parallax" data-parallax="scroll" data-image-src="img/bg11.jpg">
        <div class="container ">
            <div class="row text-center">
                <div class="col-md-12">
                    <section  class="section section-1 animated fadeInUp">
                        <h3 data-parallax="{&quot;z&quot;: 50}" class="mb-4">Benefits and features</h3>
                    </section>
                    <p data-parallax="{&quot;z&quot;: 50}" class="lead">Design unique interfaces by customizing Bootstrap theme with me.<br><br></p>
                    <div class="row text-left mt-5" data-story-tale="rotateInDownLeft, rotateInDownRight">
                        <div data-parallax="{&quot;z&quot;:30}" class="col-md-4 my-3">
                            <div class="row mb-3">
                                <div class="text-center col-2"><i class="d-block mx-auto fa fa-3x fa-mars"></i></div>
                                <div class="align-self-center col-10">
                                    <h5 class=""><b>For him</b></h5>
                                </div>
                            </div>
                            <p>I specialise in Front End Development and I create very efficient, mobile-first responsive websites, well browser supported and clean front end code. Writing semantic markup code that is well documented and easy to read, which allows co-workers to quickly work with it.</p>
                        </div>
                        <div data-parallax="{&quot;z&quot;:30}" class="col-md-4 my-3 ">
                            <div class="row mb-3">
                                <div class="text-center col-2"><i class="d-block mx-auto fa fa-3x fa-venus"></i></div>
                                <div class="align-self-center col-10">
                                    <h5><b>For her</b></h5>
                                </div>
                            </div>
                            <p>Rather than create websites on a page-by-page basis with a lot of repetitive code, I consider each element of your screen-based designs as separate components that can exist elsewhere on a website</p>
                        </div>
                        <div data-parallax="{&quot;z&quot;:30}" class="col-md-4 my-3">
                            <div class="row mb-3">
                                <div class="text-center col-2"><i class="d-block mx-auto fa fa-3x fa-neuter"></i></div>
                                <div class="align-self-center col-10">
                                    <h5><b>For its</b></h5>
                                </div>
                            </div>
                            <p>With these components I produce a style guide that acts as a document for a reusable and maintainable code base which causes your site load faster and using flawless code.</p>
                        </div>
                        <div data-parallax="{&quot;z&quot;:-30}" class="col-md-4 my-3">
                            <div class="row mb-3">
                                <div class="text-center col-2"><i class="d-block mx-auto fa fa-3x fa-mars-stroke"></i></div>
                                <div class="align-self-center col-10">
                                    <h5><b>For anyone</b></h5>
                                </div>
                            </div>
                            <p>My Front End build process involves the use of tools such as: HTML5 (deep knowledge of proper HTML structure, rich snippets)</p>
                        </div>
                        <div data-parallax="{&quot;z&quot;:-30}" class="col-md-4 my-3">
                            <div class="row mb-3">
                                <div class="text-center col-2"><i class="d-block mx-auto fa fa-3x fa-mars-double"></i></div>
                                <div class="align-self-center col-10">
                                    <h5><b>Even couples</b></h5>
                                </div>
                            </div>
                            <p>CSS3 (deep knowledge of CSS structuring, animations etc.) <br>SASS (Syntactically Awesome Style Sheets) Responsive Design PSD to HTML</p>
                        </div>
                        <div data-parallax="{&quot;z&quot;:-30}" class="col-md-4 my-3">
                            <div class="row mb-3">
                                <div class="text-center col-2"><i class="d-block mx-auto fa fa-3x fa-genderless"></i></div>
                                <div class="align-self-center col-10">
                                    <h5><b>Any other?</b></h5>
                                </div>
                            </div>
                            <p>JavaScript and jQuery (Data usage, APIs, Google analytics integration etc.) <br> Sublime Text Editor, Brackets</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>