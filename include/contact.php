<div class="container">
            <div class="row">
                <div class="col-md-6" id="contact">
                    <h1>Contact</h1>
                    <p>I would love to hear from you</p>
                    <form>
                        <div class="form-group"> <label for="InputName">Your name</label>
                            <input type="text" class="form-control" id="InputName" placeholder="Your name"> </div>
                        <div class="form-group"> <label for="InputEmail1">Email address</label>
                            <input type="email" class="form-control" id="InputEmail1" placeholder="Enter email"> </div>
                        <div class="form-group"> <label for="Textarea">Write here</label> <textarea class="form-control" id="Textarea" rows="3" placeholder="Type here"></textarea> </div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div>
        </div>