<body class="">
    <nav class="navbar navbar-expand-md bg-dark navbar-dark active" id="register">
        <a class="navbar-brand active" href="#home">Home</a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"> <span class="navbar-toggler-icon"></span> </button>
        <div class="collapse navbar-collapse " id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto" data-smooth-scroll="">
                <li class="nav-item">
                    <a class="nav-link" href="#testimonials">Testimonials</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#benifits">Benifits</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#contact">Contact</a>
                </li>
            </ul>
        </div>
       
    </nav>
    <div id="home" class="parallax" data-parallax="scroll" data-z-index="1" data-image-src="img/img-4.jpg" data-story-tale="rotateInDownLeft, rotateInDownRight"/>
            <h1 class="home-text-h1 display-3 mb-5" data-story-tale-item  data-parallax="{&quot;y&quot;: 50}">FRONT END DEVELOPER</h1>
            <p class="home-text-p1 lead mb-5"  data-story-tale-container="fadeInRightBig" data-parallax="{&quot;x&quot;: -150}">I CREATE HIGH-END, INTERACTIVE AND RESPONSIVE WEB SITES, WRITING CLEAN FRONT END CODE.</p>
            
        
    </div>
    
