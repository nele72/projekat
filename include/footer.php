<div class="bg-dark foot" id="contact">
        <div class="container">
            <div class="row">
                <div class="p-4 col-md-3">
                    <h2 class="mb-4 text-info">Web Design
                        <br> </h2>
                    <p class="text-white">Whatever you may need<br>from website prototyping to publishing</p>
                </div>
                <div class="p-4 col-md-3">
                    <h2 class="mb-4 text-info">Mapsite</h2>
                    <ul class="list-unstyled">
                        <a href="#home" class="text-white">Home</a>
                        <br>
                        <a href="#testimonials" class="text-white">Testimonials</a>
                        <br>
                        <a href="#benifits" class="text-white">Benifits</a>
                        <br>
                        <a href="#contac" class="text-white">Contact</a>
                    </ul>
                </div>
                <div class="p-4 col-md-3">
                    <h2 class="mb-4 text-info">Contact</h2>
                    <p>
                        <a href="mailto:mesarosnenad@gmail.com" class="text-white"><i class="fa d-inline mr-3 fa-envelope-o text-info"></i>mesarosnenad@gmail.com</a>
                    </p>
                    <p>
                        <a href="https://goo.gl/maps/776HWGJ9eqM2" class="text-white" target="blank"><i class="fa d-inline mr-3 fa-map-marker text-info"></i>Novi Sad Serbia</a>
                    </p>
                </div>
                <div class="p-4 col-md-3 sub">
                    <h2 class="mb-4 text-info">Subscribe</h2>
                    <form>
                        <fieldset class="form-group text-white"> <label for="exampleInputEmail1">Get newsletter</label>
                            <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email"> </fieldset>
                        <button type="submit" class="btn btn-outline-info">Submit</button>
                    </form>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 mt-3">
                    <p class="text-center text-info">© Copyright 2017 Nele - All rights reserved. </p>
                </div>
            </div>
        </div>
    </div>
    <!--<script>
        $("#register a").on("click", function() {
            $("#register").find(".active").removeClass("active");
            $(this).parent().addClass("active");
        });

    </script>-->
    <script src="js/smoothscroll.js" type="text/javascript" charset="utf-8"></script>
    <script src="js/jquery.easing.1.3.js"></script>
    <script src="js/jquery.parallax-scroll.js"></script>
     <script src="story-tale/jquery.js"></script>
  
  <script src="story-tale/story-tale.js"></script>

  


</body>

</html>
