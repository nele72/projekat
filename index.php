
    <?php include_once 'include/head.php';?>

    <?php include_once 'include/header.php';?>

    <?php include_once 'include/testimonials.php';?>
    
    <?php include_once 'include/benefits.php';?>
        <div class="py-5 text-white web">
            <div class="container">
                <div class="row">
                    <div class="align-self-center p-5 col-md-6">
                        <h1 class="mb-4" data-parallax="{&quot;z&quot;: -100}">WEB DEVELOPMENT</h1>
                        <p class="mb-5" data-parallax="{&quot;z&quot;: 100}" data-story-tale-item>Tell me your story or idea and I'll bring it to life across all devices in the best and most appropriate way suited to the device that your user using (responsive from smallest to largest devices screen - mobile, tablet, desktop - without any compatibility issues).Whether you want to build a brand, present yourself, your product or offer your services to the whole world, I will help you to create gorgeous user experience and pretty user interface design</p>
                    </div>
                    <div  class="col-md-6 ">
                        <div class="carousel-caption">
                            <h3>Build process</h3>
                            <p>Good WEB DEVELOPMENT</p>
                            <h3>Proper structure</h3>
                            <p>CSS3 HTML5</p>
                            <h3>Animations</h3>
                            <p>JavaScript and jQuery</p>
                        </div>
                     </div>
                 </div>
            </div>
        </div>
    <div class="friend py-5" id="about">
        <div class="text-center">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 love" data-parallax="{&quot;y&quot;: 50}" data-story-tale="rotateInDownLeft, rotateInDownRight" animated fadeInUpBig story-tale-served>
                        <h1 class="display-4 mb-5" data-story-tale-item>I</h1>
                        <h1 class="display-4 mb-5 srce" data-story-tale-item>♥</h1>
                        <h1  class="display-4 mb-5" data-story-tale-item>New Friends</h1>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12" data-parallax="{&quot;y&quot;: 50}">
                        <a href="https://www.facebook.com/" class="text-dark" target="blank"><i class="fa fa-fw fa-facebook fa-3x mx-3"></i></a>
                        <a href="https://twitter.com/" class="text-dark" target="blank"><i class="fa fa-fw fa-twitter fa-3x mx-3"></i></a>
                        <a href="https://plus.google.com/" class="text-dark" target="blank"><i class="fa fa-fw fa-3x fa-google-plus mx-3"></i></a>
                        <a href="https://www.instagram.com/" class="text-dark" target="blank"><i class="fa fa-fw fa-instagram fa-3x mx-3"></i></a>
                    </div>
                </div>
            </div>
        </div>
        <?php include_once 'include/contact.php';?>
    </div>
    <?php include_once 'include/footer.php';?>